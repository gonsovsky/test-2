﻿using System.Data.Entity;

namespace WebMoney.DataAccess
{

    public class WebMoneyInitializer : CreateDatabaseIfNotExists<WebMoneyContext>
    {
        protected override void Seed(WebMoneyContext context)
        {
            context.Database.ExecuteSqlCommand(
                @"

CREATE UNIQUE INDEX IX_GetOrAdd_Name ON GetOrAdds (Name)
"
);

            context.Database.ExecuteSqlCommand(
                @"

create procedure [dbo].[sp_GetOrAdd] (@name varchar(50), @id int output)
as
begin
	set XACT_ABORT on
	begin tran
	    select @ID = ID from GetOrAdds with (holdlock, updlock) WHERE name=@name
	    if @@rowcount = 0
	    begin
	        INSERT INTO GetOrAdds(name) values(@name);
	        set @ID = @@Identity;
	    end
	commit tran
end

"
);


            context.Database.ExecuteSqlCommand(
                @"

create procedure [dbo].[sp_Transfer] (@id1 int, @id2 int, @amount money)

AS
BEGIN
	SET NOCOUNT ON;
	Update Transfers set balance = balance + @amount * (case id when @id1 then 1 when @id2 then -1 else 0 end)
		where id=@id1 or id=@id2
END

"
);

            context.Database.ExecuteSqlCommand(
                @"

            create procedure sp_AddOrUpdate(@id int, @value int)
as
begin

    merge AddOrUpdates with(HOLDLOCK) as target

    using (values(@value))

        as source (value)
        on target.id = @id

    when matched then
        update

        set value = @value


    when not matched then

        insert(id, value)

        values(@id, @value);
            end
"
);
            context.Database.ExecuteSqlCommand(
                @"
    insert into Transfers (id,balance) values (1,100)
");

            context.Database.ExecuteSqlCommand(
                @"
    insert into Transfers (id,balance) values (2,100)
");

        }

    }
}

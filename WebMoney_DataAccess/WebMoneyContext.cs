﻿using System.Data.Entity;
using WebMoney.Model;

namespace WebMoney.DataAccess
{
    public class WebMoneyContext : DbContext
    {
        public WebMoneyContext()
            : base("name=DbConnectionString")
        {
            Database.SetInitializer(new WebMoneyInitializer());
        }



        public DbSet<GetOrAdd> GetOrAdd { get; set; }

        public DbSet<AddOrUpdate> AddOrUpdate { get; set; }

        public DbSet<Transfer> Transfer { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebMoney.TestConsoleApp.WebMoneyService;

namespace WebMoney.TestConsoleApp
{
    class Program
    {
        //WCF-Service
        static IWebMoneyService svc = null;

        //3 домашних задания (см. ниже). WorkMode - SQL или ORM режим.
        static readonly Action<WorkMode, int>[] Exercises = new Action<WorkMode, int>[]
                {
                    GetOrAdd
                    ,AddOrUpdate
                    ,Transfer
                };

        static void Main(string[] args)
        {
            svc = new WebMoneyServiceClient();
            //пробежимся по всем домашним заданиям 
            foreach (var exercise in Exercises)
            {
                //Пробежимя по двум режимам работы с базой: SQL/ORM
                foreach (var workMode in Enum.GetValues(typeof(WorkMode)).Cast<WorkMode>())
                {
                    //запустим задание 10 раз параллельно
                    var tasks = new List<Task>();
                    for (int i = 1; i <= 5; i++)
                    {
                        var testValue = i;
                        var task = Task.Factory.StartNew(
                            () => { exercise(workMode, testValue);  });
                        tasks.Add(task);

                    }
                    Task.WaitAll(tasks.ToArray());
                }

            }
            Console.WriteLine("Press enter to exit");
            Console.ReadLine();
        }

        static void GetOrAdd(WorkMode workMode, int testValue)
        {
            var val = testValue;
            var valStr = val.ToString();
            var result = svc.GetOrAdd(workMode, valStr);
        }

        static void AddOrUpdate(WorkMode workMode, int testValue)
        {
            svc.AddOrUpdate(workMode, testValue, testValue);
        }

        static void Transfer(WorkMode workMode, int testValue)
        {
            svc.Transfer(workMode, 1, 2, testValue);
        }
    }
}

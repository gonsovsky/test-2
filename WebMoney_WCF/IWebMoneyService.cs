﻿using System.Runtime.Serialization;
using System.ServiceModel;

namespace WebMoney.WCF
{
    [ServiceContract]
    [ServiceKnownType(typeof(WorkMode))]
    public interface IWebMoneyService
    {
        /// <summary>
        /// Решение задачи N1
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [OperationContract]
        int GetOrAdd(WorkMode workMode, string name);

        /// <summary>
        /// Решение задачи N2
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        [OperationContract]
        void AddOrUpdate(WorkMode workMode, int id, int value);

        /// <summary>
        /// Решение задачи N3
        /// </summary>
        /// <param name="id1"></param>
        /// <param name="id2"></param>
        /// <param name="amount"></param>
        [OperationContract]
        void Transfer(WorkMode workMode, int id1, int id2, decimal amount);


    }

    [DataContract]
    public enum WorkMode
    {
        [EnumMember]
        SQL,

        [EnumMember]
        ORM
    }

}

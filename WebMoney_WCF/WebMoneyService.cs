﻿using System.Collections.Generic;

namespace WebMoney.WCF
{

    public class WebMoneyService : IWebMoneyService
    {
        public int GetOrAdd(WorkMode workMode, string name)
        {
            return this[workMode].GetOrAdd(workMode, name);
        }

        public void AddOrUpdate(WorkMode workMode, int id, int value)
        {
            this[workMode].AddOrUpdate(workMode, id, value);
        }

        public void Transfer(WorkMode workMode, int id1,  int id2,  decimal amount)
        {
            this[workMode].Transfer(workMode, id1, id2, amount);
        } 

        protected IWebMoneyService this[WorkMode workMode]
        {
            get
            {
                IWebMoneyService res = null;
                if (svcArr.TryGetValue(workMode, out res))
                    return res;
                switch (workMode)
                {
                    case WorkMode.ORM: res = new WebMoneyServiceORM(); break;
                    case WorkMode.SQL: res = new WebMoneyServiceSQL(); break;    
                }
                svcArr.Add(workMode, res);   
                return res;
            }
        }

        private Dictionary<WorkMode, IWebMoneyService> svcArr = new Dictionary<WorkMode, IWebMoneyService>();

    }
}

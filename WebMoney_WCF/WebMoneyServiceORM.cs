﻿using System.Data.Entity;
using System.Linq;
using WebMoney.DataAccess;
using WebMoney.Model;
using System.Data.Entity.Infrastructure;

namespace WebMoney.WCF
{
    public class WebMoneyServiceORM : IWebMoneyService
    {
        public int GetOrAdd(WorkMode workMode, string name)
        {
            using (var context = new WebMoneyContext())
            {
                var rec = context.GetOrAdd.
                    FirstOrDefault(e => e.Name.CompareTo(name)==0);
                if (rec == null)
                {
                    var t = new GetOrAdd() { Name = name };
                    context.GetOrAdd.Add(t);
                    context.SaveChanges();
                    return t.Id;
                }
                else
                {
                    return rec.Id;
                }
            }
        }


        public void AddOrUpdate(WorkMode workMode, int id, int value)
        {
            using (var context = new WebMoneyContext())
            {
                try
                {
                    var rec = context.AddOrUpdate.Find(new object[] { id });
                    if (rec == null)
                    {
                        var t = new AddOrUpdate() { Id = id, Value = value };
                        context.AddOrUpdate.Add(t);
                        context.SaveChanges();
                    }
                    else
                    {
                        if (rec.Value != value)
                        {
                            rec.Value = value;
                            context.Entry(rec).State = EntityState.Modified;
                            context.SaveChanges();
                        }
                    }
                }
                catch(DbUpdateConcurrencyException ex)
                {
                    throw ex;
                }
            }
        }

        public void Transfer(WorkMode workMode, int id1, int id2, decimal amount)
        {
            using (var context = new WebMoneyContext())
            {
                try
                {
                    var r1 = context.Transfer.Find(new object[] { id1 });
                    var r2 = context.Transfer.Find(new object[] { id2 });
                    r1.Balance += amount;
                    r2.Balance -= amount;
                    context.SaveChanges();
                }
                catch(DbUpdateConcurrencyException ex)
                {
                    throw ex;
                }
            }
        }
    }
}

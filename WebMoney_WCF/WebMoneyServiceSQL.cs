﻿using System.Data.SqlClient;
using System.Data;
using WebMoney.DataAccess;

namespace WebMoney.WCF
{
    public class WebMoneyServiceSQL : IWebMoneyService
    {
        public int GetOrAdd(WorkMode workMode, string name)
        {
            using (var context = new WebMoneyContext())
            {
                var nameP = new SqlParameter("@name", SqlDbType.VarChar);
                nameP.Value = name;
                var idP = new SqlParameter("@id", SqlDbType.Int);
                idP.Direction = ParameterDirection.Output;
                var result = context.Database.ExecuteSqlCommand(
                    @"exec dbo.sp_GetOrAdd @name, @id OUTPUT", nameP, idP
                    );
                return (int)(idP.Value);
            }
        }

        public void AddOrUpdate(WorkMode workMode, int id, int value)
        {
            using (var context = new WebMoneyContext())
            {
                var idP = new SqlParameter("@id", SqlDbType.Int);
                idP.Value = id;
                var valP = new SqlParameter("@value", SqlDbType.Int);
                valP.Value = value;

                context.Database.ExecuteSqlCommand(
                    @"exec dbo.sp_AddOrUpdate  @id, @value", idP, valP
                    );
            }
        }

        public void Transfer(WorkMode workMode, int id1, int id2, decimal amount)
        {
            using (var context = new WebMoneyContext())
            {
                var id1P = new SqlParameter("@id1", SqlDbType.Int);
                id1P.Value = id1;
                var id2P = new SqlParameter("@id2", SqlDbType.Int);
                id2P.Value = id2;
                var amountP = new SqlParameter("@amount", SqlDbType.Money);
                amountP.Value = amount;

                context.Database.ExecuteSqlCommand(
                    @"exec dbo.sp_Transfer  @id1, @id2, @amount", id1P, id2P, amountP
                    );
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WebMoney.WCF.Lib
{
    [ServiceContract]
    [ServiceKnownType(typeof(WorkingModeEnum))]
    public interface IWebMoneyService
    {



        /// <summary>
        /// Решение задачи N1
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [OperationContract]
        int Task1_GetOrAdd(WorkingModeEnum mode, string name);  
    }


    /// <summary>
    /// Режим работы сервиса с базой: SQL или ORM
    /// </summary>
    [DataContract]
    public enum WorkingModeEnum
    {
        /// <summary>
        /// Режим работы SQL
        /// </summary>
        [EnumMember]
        SQL,

        /// <summary>
        /// Режим работы ORM
        /// </summary>
        [EnumMember]
        ORM
    };

    
}

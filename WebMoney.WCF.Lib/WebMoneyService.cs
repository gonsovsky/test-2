﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WebMoney.DataAccess;
using WebMoney.Model;

namespace WebMoney.WCF.Lib
{
    public class WebMoneyService : IWebMoneyService
    {

        /// <summary>
        /// Решение задачи N1
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public int Task1_GetOrAdd(WorkingModeEnum mode, string name)
        {
            var t = new Table1();
            t.Name = "123";
            context.Table1.Add(t);
            context.SaveChanges();
            return 0;
        }

        internal WebMoneyContext context;

        public WebMoneyService()
        {
            context = new WebMoneyContext();
            var strategy = new DropCreateDatabaseIfModelChanges<WebMoneyContext>();
            Database.SetInitializer(strategy);
           
        }




    }
}
